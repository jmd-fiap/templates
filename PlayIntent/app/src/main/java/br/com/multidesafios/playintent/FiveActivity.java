package br.com.multidesafios.playintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class FiveActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_five);
    }

    public void goToOne(View v){
        Intent toOneActivity = new Intent(this, OneActivity.class);
        startActivity(toOneActivity);
    }

    public void goToTwo(View v){
        Intent toTwoActivity = new Intent(this, TwoActivity.class);
        startActivity(toTwoActivity);
    }

    public void goToThree(View v){
        Intent toThreeActivity = new Intent(this, ThreeActivity.class);
        startActivity(toThreeActivity);
    }

    public void goToFour(View v){
        Intent toFourActivity = new Intent(this, FourActivity.class);
        startActivity(toFourActivity);
    }

    public void goToFive(View v){
        Intent toFiveActivity = new Intent(this, FiveActivity.class);
        startActivity(toFiveActivity);
    }

}
